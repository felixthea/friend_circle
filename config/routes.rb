FriendCircle::Application.routes.draw do
  resources :users do
    collection do
      get 'forgot_password'
      post 'email_password_reset'
      get 'password_reset'
      get 'feed'
    end
  end

  resource :session
  resources :circles
  resources :posts
end
