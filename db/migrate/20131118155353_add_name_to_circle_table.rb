class AddNameToCircleTable < ActiveRecord::Migration
  def change
    add_column :circles, :name, :string, null: false
  end
end
