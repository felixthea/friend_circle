class PostsController < ApplicationController

  def show
    @post = Post.find(params[:id])
    @links = @post.links
    render :show
  end

  def index
    @posts = Post.all
    render :index
  end

  def new
    @post = Post.new
    # 3.times { @post.links.new }
    @urls = ["","",""]
    render :new
  end

  def create
    params[:post][:user_id] = current_user.id
    @post = Post.new(params[:post])
    @post.post_shares.new(params[:circle_shares].values)
    @urls = params[:links].values.select { |hsh| hsh["url"].length > 0 }
    @post.links.new(@urls)

    if @post.save
      redirect_to post_url(@post)
    else
      flash.now[:errors] = "Your post wasn't created."
      render :new
    end
  end

  def edit
    @post = Post.find(params[:id])
    @urls = @post.links.map { |link| link.url }
    render :edit
  end

  def destroy
  end
end
