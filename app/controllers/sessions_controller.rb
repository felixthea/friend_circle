class SessionsController < ApplicationController

  skip_before_filter :require_logged_in, only: [:new, :create]
  before_filter :require_logged_out, only: [:new, :create]

  def new
    render :new
  end

  def create
    email = params[:user][:email]
    password = params[:user][:password]

    user = User.find_by_credentials(email, password)

    if user.nil?
      flash[:errors] = "Invalid email or password"
      redirect_to new_session_url
    else
      log_in_user!(user)
      redirect_to user_url(user)
    end
  end

  def destroy
    log_out_user!(current_user)
    redirect_to new_session_url
  end
end
