class CirclesController < ApplicationController

  def index
    @circles = Circle.all

    render :index
  end

  def show
    @circle = Circle.find(params[:id])
    @members = @circle.members

    render :show
  end

  def new
    @circle = Circle.new
    render :new
  end

  def create
    params[:circle][:user_id] = current_user.id
    circle = Circle.new(params[:circle])

    if circle.save
      redirect_to circle_url(circle)
    else
      flash.now[:errors] = "Your circle wasn't created."
      render :new
    end
  end

  def edit
    @circle = Circle.find(params[:id])
  end

  def update
    @circle = Circle.find(params[:id])
    if @circle.update_attributes(params[:circle])
      redirect_to circle_url(@circle)
    else
      flash.now[:errors] = "Your circle wasn't updated."
      render :edit
    end
  end

  def destroy
    circle = Circle.find(params[:id])
    if current_user == circle.owner
      circle.destroy
    else
      flash.now[:errors] = "Can't delete circles you didn't create"
    end
    redirect_to user_url(current_user)
  end

end
