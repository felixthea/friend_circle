class UsersController < ApplicationController

  skip_before_filter :require_logged_in,
    only: [:new, :create, :forgot_password, :password_reset, :email_password_reset]
  before_filter :require_logged_out,
    only: [:new, :create, :forgot_password, :password_reset, :email_password_reset]

  def new
    render :new
  end

  def create
    @user = User.new(params[:user])
    @post = @user.posts.new(params[:post])
    @urls = params[:links].values.select { |hsh| hsh["url"].length > 0 }
    @post.links.new(@urls)
    if @user.save
      log_in_user!(@user)
      redirect_to user_url(@user)
    else
      flash[:errors] = "Invalid email or password."
      redirect_to new_user_url
    end
  end

  def show
    @user = User.find(params[:id])

    render :show
  end

  def forgot_password
    render :forgot_password
  end

  def email_password_reset
    user = User.find_by_email(params[:email])
    msg = UserMailer.forgot_password_email(user)
    msg.deliver!
  end

  def password_reset
    user = User.find_by_session_token(params[:q])
    @new_password = SecureRandom.urlsafe_base64(6)
    user.password = @new_password
    user.save

    render :password_reset
  end

  def feed
    @posts = current_user.feed_posts
    render :feed
  end
end
