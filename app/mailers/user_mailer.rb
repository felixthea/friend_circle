class UserMailer < ActionMailer::Base
  default from: "from@example.com"

  def forgot_password_email(user)
    @user = user
    @url = Addressable::URI.new(
    :scheme => "http",
       :host => "localhost",
       :port => "3000",
       :path => "users/password_reset",
       :query_values => {:q => "#{user.session_token}"}
    ).to_s

    mail(to: user.email, subject: 'Forgot Password')
  end
end
