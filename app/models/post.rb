class Post < ActiveRecord::Base
  attr_accessible :user_id, :body, :title

  belongs_to(
    :author,
    class_name: "User",
    foreign_key: :user_id,
    primary_key: :id,
    inverse_of: :posts
  )

  has_many(
    :links,
    class_name: "Link",
    foreign_key: :post_id,
    primary_key: :id,
    dependent: :destroy,
    inverse_of: :post
  )

  has_many(
    :post_shares,
    class_name: "PostShare",
    foreign_key: :post_id,
    primary_key: :id,
    dependent: :destroy,
    inverse_of: :post
  )

  has_many :circles, through: :post_shares, source: :circle

  validates :author, :body, :title, presence: true
end
