class User < ActiveRecord::Base
  attr_accessible :email, :password
  attr_reader :password

  has_many(
    :circles,
    class_name: "Circle",
    foreign_key: :user_id,
    primary_key: :id,
    dependent: :destroy
  )

  has_many(
    :memberships,
    class_name: "CircleMembership",
    foreign_key: :user_id,
    primary_key: :id,
    dependent: :destroy
  )

  has_many(
    :posts,
    class_name: "Post",
    foreign_key: :user_id,
    primary_key: :id,
    dependent: :destroy,
    inverse_of: :author
  )

  has_many(
    :circles_user_belongs_to,
    through: :memberships,
    source: :circle
  )

  has_many(
    :feed_posts,
    through: :circles_user_belongs_to,
    source: :posts_shared_with_a_circle
  )

  before_validation :ensure_session_token
  validates :email, :password_digest, :session_token, presence: true
  validates :email, uniqueness: true
  validates :password, length: { minimum: 6, allow_nil: true }

  def self.find_by_credentials(email, secret)
    user = User.find_by_email(email)
    return nil if user.nil?

    user.is_password?(secret) ? user : nil
  end

  def password=(secret)
    self.password_digest = BCrypt::Password.create(secret)
  end

  def is_password?(secret)
    BCrypt::Password.new(password_digest).is_password?(secret)
  end

  def reset_session_token!
    self.session_token = self.class.generate_session_token
    self.save!
  end

  private

  def self.generate_session_token
    SecureRandom.urlsafe_base64(16)
  end

  def ensure_session_token
    self.session_token ||= self.class.generate_session_token
  end
end
