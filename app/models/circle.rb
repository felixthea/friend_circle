class Circle < ActiveRecord::Base
  attr_accessible :user_id, :name, :member_ids

  has_many(
    :memberships,
    class_name: "CircleMembership",
    foreign_key: :circle_id,
    primary_key: :id,
    inverse_of: :circle,
    dependent: :destroy
  )

  belongs_to(
    :owner,
    class_name: "User",
    foreign_key: :user_id,
    primary_key: :id
  )

  has_many(
    :post_shares,
    class_name: "PostShare",
    foreign_key: :circle_id,
    primary_key: :id,
    dependent: :destroy,
  )

  has_many(
    :posts_shared_with_a_circle,
    through: :post_shares,
    source: :post
  )

  has_many :members, through: :memberships, source: :member

  validates :user_id, :name, presence: true

end
